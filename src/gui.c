/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gui.h"


/* =============== G U I   I N I T I A L I S A T I O N ============== */

/* Call all gui init functions */
void gui_init (Gui *gui)
{
    gui_init_window (gui);
    gui_init_layout (gui);
    gui_init_drawing_area (gui);
    gui_init_boxes_pack (gui);
    gui_init_g_signals (gui);

    /* Show the main and the stat window on toplevel */
    gtk_widget_show_all (GTK_WIDGET (gui->window));
    gtk_widget_show_all (GTK_WIDGET (gui->stat_window));
}

void gui_init_window (Gui *gui)
{
    /* Init the main window where the animation is played */
    gui->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (gui->window), "Neko & Mausu");
    gtk_window_set_default_size (GTK_WINDOW (gui->window),
                         gui->info->area_width, gui->info->area_height);

    /* Init the secondary window where stats are printed */
    gui->stat_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (gui->stat_window), "Game's stat");
    gtk_window_set_default_size (GTK_WINDOW (gui->stat_window),
                                                              315, 600);
}

void gui_init_boxes (Gui *gui)
{
    /* Main window vbox which contain the area and the top bar for the
     * main window */
    gui->layout.main_box = gtk_vbox_new (!HOMOGENEOUS, 1);
    gtk_container_add (GTK_CONTAINER (gui->window),
                       gui->layout.main_box);

    /* Top hbox which contain some buttons */
    gui->layout.top_box = gtk_hbox_new (!HOMOGENEOUS, 5);

    /* Stat window vbox which contain the drawing area */
    gui->layout.stat_box = gtk_vbox_new (!HOMOGENEOUS, 1);
    gtk_container_add (GTK_CONTAINER (gui->stat_window),
                       gui->layout.stat_box);
}

/* Buttons to manage the next neko's parameter */
void gui_init_next_bandit_buttons (Gui *gui)
{
    Layout *l;
    NextBandit * next_bandit;

    l = & gui->layout;
    next_bandit = & gui->info->next_bandit;

    /* Algorithm combobox with every algorithm choice */
    l->next_bandit_algo = gtk_combo_box_new_text ();
    gtk_combo_box_append_text (GTK_COMBO_BOX (l->next_bandit_algo),
                                                           "UCB 1");
    gtk_combo_box_append_text (GTK_COMBO_BOX (l->next_bandit_algo),
                                                           "UCB 2");
    gtk_combo_box_append_text (GTK_COMBO_BOX (l->next_bandit_algo),
                                                           "E-Glouton");
    gtk_combo_box_append_text (GTK_COMBO_BOX (l->next_bandit_algo),
                                                           "E-Greedy");
    gtk_combo_box_append_text (GTK_COMBO_BOX (l->next_bandit_algo),
                                                           "EXP 3");
    gtk_combo_box_set_active  (GTK_COMBO_BOX (l->next_bandit_algo), 0);

    /* Arms number spin button from 4 to MAX_ARMS */
    l->next_bandit_arm = gtk_spin_button_new_with_range (4, MAX_ARMS, 1);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (l->next_bandit_arm),
                               next_bandit->arm);

    /* Speed spin button from 5 to 15 */
    l->next_bandit_speed = gtk_spin_button_new_with_range (5, 15, 1);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (l->next_bandit_speed),
                               next_bandit->speed);

    /* Alpha spin button from 0.01 to 10 */
    l->next_bandit_alpha =
                        gtk_spin_button_new_with_range (0.01, 10, 0.01);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (l->next_bandit_alpha),
                               next_bandit->alpha);

    /* Color button used for the next neko */
    l->next_bandit_color = gtk_color_button_new_with_color (
                              static_color_from_rgb (next_bandit->color));
}

/* Init buttons and boxes */
void gui_init_layout (Gui *gui)
{
    Layout *l;
    l = & gui->layout;

    gui_init_boxes (gui);
    gui_init_next_bandit_buttons (gui);

    l->start_button = gtk_button_new_with_label ("Start");
    l->next_button  = gtk_button_new_with_label ("Next");
    l->reset_button = gtk_button_new_with_label ("Reset");
    l->stat_scroll = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_set_border_width (GTK_CONTAINER (l->stat_scroll), 10);
}

/* Pack the layout in horizontal and vertical boxes */
void gui_init_boxes_pack (Gui *gui)
{
    GtkWidget *label_name;
    Layout *l;
    l = & gui->layout;

    /* Pack the drawing area and the button box on the main box */
    gtk_box_pack_start (GTK_BOX (l->main_box), l->top_box,
                        !EXPAND, FILL, 0);
    gtk_box_pack_start (GTK_BOX (l->main_box), l->area,
                        EXPAND, FILL, 0);

    /* Add start/next/reset buttons on the top box of the main window */
    gtk_box_pack_start (GTK_BOX (l->top_box), l->start_button,
                        !EXPAND, FILL, 0);
    gtk_box_pack_start (GTK_BOX (l->top_box), l->next_button,
                        !EXPAND, FILL, 0);
    gtk_box_pack_start (GTK_BOX (l->top_box), l->reset_button,
                        !EXPAND, FILL, 0);

    /* Add nextneko buttons on the top box of the main window */
    gtk_box_pack_end (GTK_BOX (l->top_box), l->next_bandit_color,
                      !EXPAND, FILL, 0);

    label_name = gtk_label_new ("Arm(s):");
    gtk_box_pack_end (GTK_BOX (l->top_box), l->next_bandit_arm,
                      !EXPAND, FILL, 0);
    gtk_box_pack_end (GTK_BOX (l->top_box), label_name,
                      !EXPAND, FILL, 0);

    label_name = gtk_label_new ("Speed:");
    gtk_box_pack_end (GTK_BOX (l->top_box), l->next_bandit_speed,
                      !EXPAND, FILL, 0);
    gtk_box_pack_end (GTK_BOX (l->top_box), label_name,
                      !EXPAND, FILL, 0);

    label_name = gtk_label_new ("Alpha:");
    gtk_box_pack_end (GTK_BOX (l->top_box), l->next_bandit_alpha,
                      !EXPAND, FILL, 0);
    gtk_box_pack_end (GTK_BOX (l->top_box), label_name,
                      !EXPAND, FILL, 0);

    label_name = gtk_label_new ("Algo:");
    gtk_box_pack_end (GTK_BOX (l->top_box), l->next_bandit_algo,
                      !EXPAND, FILL, 0);
    gtk_box_pack_end (GTK_BOX (l->top_box), label_name,
                      !EXPAND, FILL, 0);

    /* Pack the drawing area on the stat box */
    gtk_box_pack_start (GTK_BOX (l->stat_box), l->stat_area,
                        EXPAND, FILL, 0);
}

void gui_init_drawing_area (Gui *gui)
{
    Layout *l;
    l = & gui->layout;

    /* Main window init drawing area */
    gui->layout.area = gtk_drawing_area_new ();
    GTK_WIDGET_SET_FLAGS  (l->area, GTK_CAN_FOCUS);
    gtk_widget_add_events (l->area, GDK_EXPOSURE_MASK);

    /* Stat window init drawing area */
    l->stat_area = gtk_drawing_area_new ();
    GTK_WIDGET_SET_FLAGS  (l->stat_area, GTK_CAN_FOCUS);
    gtk_widget_add_events (l->stat_area, GDK_EXPOSURE_MASK);
}

/* Init all G_signals */
void gui_init_g_signals (Gui *gui)
{
    Layout *l;
    Info *info;

    l = & gui->layout;
    info = gui->info;

    gtk_widget_add_events (l->area, GDK_BUTTON_PRESS_MASK);

    /* Main window callbacks */
    ON_SIGNAL (gui->window, "delete-event", gtk_main_quit, NULL);
    ON_SIGNAL (gui->window, "destroy", gtk_main_quit, NULL);
    ON_SIGNAL (l->area,     "expose-event",
                                            on_area_expose_event, gui);
    ON_SIGNAL (l->area,     "configure-event",
                                            on_area_configure, gui);
    ON_SIGNAL (l->area,     "button-press-event",
                                            on_area_button_press, gui);

    /* Expose events for the main window's buttons */
    ON_SIGNAL (l->start_button, "clicked", on_button_start, gui);
    ON_SIGNAL (l->next_button,  "clicked", on_button_next, gui);
    ON_SIGNAL (l->reset_button, "clicked", on_button_reset, gui);

    /* Expose events for the nextneko settings */
    ON_SIGNAL (l->next_bandit_color, "color_set",
                                            on_next_bandit_color, gui);
    ON_SIGNAL (l->next_bandit_algo,  "changed",
                                            on_next_bandit_algo, info);
    ON_SIGNAL (l->next_bandit_speed, "value-changed",
                                            on_next_bandit_speed, gui);
    ON_SIGNAL (l->next_bandit_arm,   "value-changed",
                                            on_next_bandit_arm, gui);
    ON_SIGNAL (l->next_bandit_alpha, "value-changed",
                                            on_next_bandit_alpha, gui);

    /* Expose event for the stat window */
    ON_SIGNAL (l->stat_area, "expose-event",
                                       on_stat_area_expose_event, info);
}
