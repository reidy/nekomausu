/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "algo.h"


/* ============================= A L G O ============================ */

/* UCB1 algorithm */
void UCB1 (Bandit *b, float exploration, Info *info)
{
    /* Initialize every arms or select the best one */
    (b->arm < b->time) ?
                    algo_play (b, algo_best_try (b, exploration), info)
                    : algo_play (b, b->time, info);

    b->time++;
}

/* UCB2 algorithm */
void UCB2 (Bandit *b, float alpha, Info *info)
{
    /* Initialize every arms or select the best one */
    (b->arm < b->time) ?
                algo_UCB2_play (b, algo_UCB2_best_try (b, alpha), alpha)
                : algo_play (b, b->time, info);

    b->time++;
}

/* E-glouton algorithm */
void eglouton (Bandit *b, float epsilon, Info *info)
{
    /* Prevent from choosing best try arm */
    (drand () < epsilon) ? algo_play (b, rand() % b->arm, info)
                         : algo_play (b, algo_best_try (b, 0), info);

    b->time++;
}

/* E-greedy algorithm */
void egreedy (Bandit *b, float c, float d, Info *info)
{
    float epsilon;

    /* Special formula for epsilon */
    epsilon = (c * b->arm / (d * d * b->time) > 1) ? 1
                                     : (c * b->arm / (d * d * b->time));

    /* Prevent from choosing best try arm */
    (drand () < epsilon) ? algo_play (b, rand() % b->arm, info)
                         : algo_play (b, algo_best_try (b, 0), info);

    b->time++;
}

/* Exploration, Exploitation, Exponential algorithm */
void EXP3 (Bandit *b, float gamma, Info *info)
{
    float P[MAX_ARMS];
    float weight_sum;
    float p_sum;
    float p_choose;
    float reward_observer;
    float estim_reward;
    int i, rand_arm;

    weight_sum = 0;
    p_sum = 0;

    /* Defines the worth of each arm */
    for (i = 0; i < b->arm; i++)
    {
        weight_sum += b->param[i].weights;
        P[i] = (1 - gamma) * b->param[i].weights / weight_sum +
                                                         gamma / b->arm;
        p_sum += P[i];
    }

    /* Choose an arm based on the worth of each arm */
    p_choose = drand () * p_sum;
    rand_arm = 0;
    while (p_sum - P[rand_arm] >= p_choose && rand_arm < b->arm)
    {
        p_sum -= P[rand_arm];
        rand_arm++;
    }

    /* Updates the values used by the algorithm */
    reward_observer = b->param[rand_arm].profit;
    algo_play (b, rand_arm, info);

    reward_observer = b->param[rand_arm].profit - reward_observer;
    estim_reward = reward_observer / P[rand_arm];
    b->param[rand_arm].weights *= exp (gamma * estim_reward / b->arm);
    b->time++;
}


/* ====================== A L G O   E V E N T S ===================== */

/* Default best try calculus used on E-Glouton, E-Greedy, UCB1 and
 * EXP3
 */
int algo_best_try (Bandit *b, float exploration)
{
    int i, best_ID;
    float best_value, temp;

    best_value = 0;
    best_ID = 0;

    for (i = 0; i < b->arm; i++)
    {
        temp = b->param[i].profit / b->param[i].choose
              + sqrt (exploration * log (b->time) / b->param[i].choose);

        if (best_value < temp)
        {
            best_value = temp;
            best_ID = i;
        }
    }

    return best_ID;
}

/* Default play events used on E-Glouton, E-Greedy and UCB1 */
void algo_play (Bandit *b, int arm, Info *info)
{
    b->last_arm_played = arm;
    bandit_reward_update (b, info);
    bandit_reward (b, arm);
}


/* ============== S P E C I A L   U C B 2   E V E N T S ============= */

/* UCB2 tau computation,
 * Using a ceil to take the value round up of 1+alpha the power of r */
int algo_UCB2_tau (int r, float alpha)
{
    if (r >= 100)
    {
        return ceil (pow (1 + alpha, 100));
    }
    return ceil (pow (1 + alpha, r));
}

/* UCB2 special best try calculus */
int algo_UCB2_best_try (Bandit *b, float alpha)
{
    int i, best_ID;
    float best_value, temp, average, anrj;

    best_value = INT_MIN;
    best_ID = 0;

    for (i = 0; i < b->arm; i++)
    {
        /* Set the exploitation */
        average = b->param[i].profit / b->param[i].choose;
        /* Euler number formula */
        anrj = sqrt (((1 + alpha) * log ((2.71 * b->time)
                    / algo_UCB2_tau (b->param[i].epoch, alpha)))
                    / (2 * algo_UCB2_tau (b->param[i].epoch, alpha)));
        temp = average + anrj;

        if (best_value < temp)
        {
            best_value = temp;
            best_ID = i;
        }
    }

    return best_ID;
}

/* UCB2 special play events */
void algo_UCB2_play (Bandit *b, int arm, float alpha)
{
    int j;
    int play_time;

    play_time = algo_UCB2_tau (b->param[arm].epoch + 1, alpha) -
                algo_UCB2_tau (b->param[arm].epoch, alpha);

    for (j = 0; j < play_time; j++)
    {
        bandit_reward (b, arm);
    }

    b->param[arm].epoch++;
    b->param[arm].choose += j;
    b->time += j;
    b->last_arm_played = arm;
}
