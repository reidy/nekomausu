/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GUI_H__
#define __GUI_H__

#include "conf.h"

/* Gui initialisation */
void gui_init (Gui *gui);
void gui_init_window (Gui *gui);
void gui_init_boxes (Gui *gui);
void gui_init_layout (Gui *gui);
void gui_init_boxes_pack (Gui *gui);
void gui_init_drawing_area (Gui *gui);
void gui_init_g_signals (Gui *gui);

#endif
