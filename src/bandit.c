/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bandit.h"


/* =================== G A M E   M E C H A N I S M ================== */

/* Algorithm loop */
void bandit_game (Info *info)
{
    Bandit *neko, *mausu;
    int cptr;

    neko = & info->nekos->neko;
    mausu = & info->mausu;

    bandit_choose_algorithm (mausu, info);
    bandit_move (mausu, info);

    for (cptr = 0; cptr < info->size_list; cptr++)
    {
        neko = bandit_get_number (info->nekos, cptr);

        if (dist (neko->pos.x, neko->pos.y,
                  mausu->pos.x, mausu->pos.y) > SIZE)
        {
            bandit_choose_algorithm (neko, info);
            bandit_move (neko, info);
        }
        else
        {
            neko->winner = 1;
            info_anim_stop (info);
        }
    }
}

/* Launch the n algorithm for the Bandit b */
void bandit_choose_algorithm (Bandit *b, Info *info)
{
    switch (b->algo)
    {
    case 1 : UCB1 (b, b->alpha, info);       break;
    case 2 : UCB2 (b, b->alpha, info);       break;
    case 3 : eglouton (b, b->alpha, info);   break;
    case 4 : egreedy (b, 2, b->alpha, info); break;
    case 5 : EXP3 (b, b->alpha, info);       break;
    default :
        printf ("Wrong algorithm number.\n");
        break;
    }
}

/* Update the reward */
void bandit_reward_update (Bandit *bandit, Info *info)
{
    int arm_played;
    Position aftr_move;

    arm_played = bandit->last_arm_played;
    aftr_move.x = bandit->speed * cos (PI / 2 - ((2 * PI) / bandit->arm)
                    * arm_played) + bandit->pos.x;
    aftr_move.y = bandit->speed * sin (PI / 2 - ((2 * PI) / bandit->arm)
                    * arm_played) + bandit->pos.y;

    if (bandit->is_neko)
    {
        bandit->param[arm_played].win = bandit_reward_neko (bandit,
                                                       aftr_move, info);
    }
    else
    {
        bandit->param[arm_played].win = bandit_reward_mausu (bandit->pos,
                                                       aftr_move, info);
    }
    
}

/* Gives reward and handle arrays linked with memory */
void bandit_reward (Bandit *b, int i)
{
    int changed_arm;

    if (b->memory[b->memory_index][1] != -1)
    {
        changed_arm = b->memory[b->memory_index][1];
        b->memory[b->memory_index][0] = b->param[i].win;
        b->memory[b->memory_index][1] = i;
        bandit_arm_update (b, i);
        bandit_arm_update (b, changed_arm);
        b->memory_index = (b->memory_index + 1) % BANDIT_MEMORY;
    }
    else
    {
        b->memory[b->memory_index][0] = b->param[i].win;
        b->memory[b->memory_index][1] = i;
        bandit_arm_update (b, i);
        b->memory_index = (b->memory_index + 1) % BANDIT_MEMORY;
    }
}

/* Update the profit and choose parameters */
void bandit_arm_update (Bandit *b, int i)
{
    int cntr;
    b->param[i].profit = 0;
    b->param[i].choose = 0;

    for (cntr = 0; cntr < BANDIT_MEMORY; cntr++)
    {
        if (b->memory[cntr][1] == i)
        {
            b->param[i].profit += b->memory[cntr][0];
            b->param[i].choose++;
        }
    }
}

/* Check every distance from a neko and a mausu */
float bandit_reward_neko (Bandit *neko, Position after_move, Info *info)
{
    Position pos_mausu;
    Position post_dist, pre_dist;
    float np_dist, dmax;
    float reward_distance, reward_movement;
    float reward;
    float noise;
    int width, height;

    pos_mausu = info->mausu.pos;
    width  = info->area_width;
    height = info->area_height;
    noise = drand () * (neko->speed + 1) - neko->speed / 3;

    /* Calculate how far away the mice is from the cat after movement */
    post_dist.x = dist_warp (after_move.x, pos_mausu.x, width);
    post_dist.y = dist_warp (after_move.y, pos_mausu.y, height);

    /* Before movement */
    pre_dist.x = dist_warp (neko->pos.x, pos_mausu.x, width);
    pre_dist.y = dist_warp (neko->pos.y, pos_mausu.y, height);

    /* Calculate the movement part of the reward */
    reward_movement = sqrt (pre_dist.x * pre_dist.x +
                                pre_dist.y * pre_dist.y) 
                    - sqrt (post_dist.x * post_dist.x +
                                post_dist.y * post_dist.y);

    reward_movement = exp (reward_movement + noise) /
                        exp (neko->speed * 1.5);

    /* Calculate the distance part of the reward */
    np_dist = sqrt (post_dist.x * post_dist.x +
                    post_dist.y * post_dist.y);

    dmax = sqrt (height / 2 * height / 2 + width / 2 * width / 2);
    reward_distance = np_dist / dmax;
    reward = (4 * reward_movement + reward_distance) / 5;

    return reward;
}

/* Update the mausu's position */
float bandit_reward_mausu (Position pos_before, Position pos_after, Info *info)
{
    int cptr;
    ListBandit *current;
    Position diff_pos;
    Position current_pos;
    float angles [MAX_BANDIT_NUM];
    float reward_angle;
    float size_world;
    float cm_dist;
    float noise;

    size_world = sqrt (info->area_width * info->area_width +
                        info->area_height * info->area_height) / 2;
    noise = drand () * (info->mausu.speed + 1) - info->mausu.speed / 3;

    cptr = 0;
    for (current = info->nekos; current; current = current->next)
    {
        diff_pos.x = dist_warp (pos_before.x, current->neko.pos.x,
                                info->area_width) + noise;
        diff_pos.y = dist_warp (pos_before.y, current->neko.pos.y,
                                info->area_height) + noise;

        current_pos.x = diff_pos.x + pos_before.x;
        current_pos.y = diff_pos.y + pos_before.y;

        cm_dist = sqrt (diff_pos.x * diff_pos.x +
                        diff_pos.y * diff_pos.y) / 2;

        if (cm_dist < size_world)
        {
            angles[cptr] = get_best_angle (
                                get_angle (pos_before, pos_after), 
                                get_angle (pos_before, current_pos));

            angles[cptr] *= (size_world - cm_dist) / size_world;
            cptr++;
        }
    }

    reward_angle = minimum (angles, cptr);
    reward_angle /= PI;

    return reward_angle;
}

/* Update the bandit position */
void bandit_move (Bandit *b, Info *info)
{
    int x, y;
    x = b->speed * cos (PI / 2 - (2 * PI / b->arm) *
                                                    b->last_arm_played);
    y = b->speed * sin (PI / 2 - (2 * PI / b->arm) *
                                                    b->last_arm_played);
    b->pos.x += x;
    b->pos.y += y;

    if (b->pos.x <= 0)
    {
        b->pos.x += info->area_width;
    }
    else if (b->pos.x > info->area_width)
    {
        b->pos.x -= info->area_width;
    }
    if (b->pos.y <= 0)
    {
        b->pos.y += info->area_height;
    }
    else if (b->pos.y > info->area_height)
    {
        b->pos.y -= info->area_height;
    }
}


/* =================== I N I T I A L I S A T I O N ================== */

/* Init parameters from a Bandit */
Parameters init_bandit_parameters (void)
{
    Parameters p;

    p.win = 0;
    p.choose = 0;
    p.profit = 0;
    p.epoch = 1;
    p.weights = 1;

    return p;
}

/* Init a Bandit configuration on given parameters */
void init_new_bandit (Bandit *b, NextBandit info_neko)
{
    int i;

    /* Set default values */
    b->time = 0;
    b->last_arm_played = 0;
    b->arm = info_neko.arm;
    b->speed = info_neko.speed;
    b->winner = 0;
    b->color = info_neko.color;

    /* Neko attribute */
    b->algo = info_neko.algo;
    b->alpha = info_neko.alpha;

    /* Set start Position */
    b->pos.x = info_neko.pos.x;
    b->pos.y = info_neko.pos.y;

    /* Init arms parameters */
    for (i = 0; i < b->arm; i++)
    {
        b->param[i] = init_bandit_parameters ();
    }

    /* Set default tail Positions on start Position */
    for (i = 0; i < MAX_TAIL; i++)
    {
        b->tail[i].x = b->pos.x;
        b->tail[i].y = b->pos.y;
    }

    /* Miscelaneous and memory*/
    b->memory_index = 0;
    for (i = 0; i < BANDIT_MEMORY; i++)
    {
        b->memory[i][1] = -1;
    }

    /* Hunter or hunted */
    b->is_neko = info_neko.is_neko;
}

/* Initialize default informations for the next bandit */
void init_bandit_info_toolbar (Info *info)
{
    info->next_bandit.algo = UCB_1;
    info->next_bandit.speed = DEFAULT_SPEED;
    info->next_bandit.alpha = 0.1;
    info->next_bandit.arm = 10;
    info->next_bandit.color = 0xFFCCDD;
}

/* Initialize neko(cat) and mausu(mouse) bandits */
void init_bandit_info (Info *info)
{
    info->area_width = 800;
    info->area_height = 600;
    info->anim = 0;
    info->timeout = 0;
    info->size_list = -1;

    /* Init default informations for the first Neko(cat) */
    init_bandit_info_toolbar (info);
}


/* =========================== F R E E  ============================= */

void bandit_info_free (Info *info)
{
    bandit_list_free (info->nekos, info);

    printf ("Info data cleaned.\n");
}

/* Free the linked list and get the shit out of the bandits */
void bandit_list_free (ListBandit *adress, Info *info)
{
    ListBandit *temp_bandit;

    if (info->size_list <= 0)
    {
        printf ("Nothing to free.\n");
        return;
    }

    if (adress == NULL)
    {
        perror ("Unalocated adress on bandit_list_free.");
        return;
    }
    temp_bandit = adress;

    /* Clean every nodes after the adress one */
    while (adress->next != NULL)
    {
        adress = adress->next;
        free (temp_bandit);
        info->size_list--;
        temp_bandit = adress;
    }

    /* Clean the first node */
    free (temp_bandit);
    info->size_list--;
}


/* ====================== L I N K E D   L I S T ===================== */

/* Return the last maillon of the list */
ListBandit *bandit_get_last_chain (ListBandit *current)
{
    if (current == NULL)
    {
        printf("Linked chain doesn't exist.\n");
    }

    return (current->next == NULL) ?
                        current : bandit_get_last_chain (current->next);
}

/* Return the adress of the maillon number 'number'
 * ! Beware of not sending a number superior of the size of the list !*/
Bandit *bandit_get_number (ListBandit *current, int number)
{
    return (!number) ? & current->neko
                       : bandit_get_number (current->next, number - 1);
}

/* Create a new maillon and return its adress */
ListBandit *
bandit_new_maillon (Bandit new_bandit, ListBandit *adress, Info *info)
{
    ListBandit *current;
    current = malloc (sizeof (ListBandit));

    if (current == NULL)
    {
        perror ("Memory allocation error on bandit_new_maillon");
        exit (EXIT_FAILURE);
    }

    current->neko = new_bandit;
    current->next = adress;
    info->size_list++;

    return current;
}

/* Add a new maillon on the list */
ListBandit *
bandit_add (Bandit new_bandit, ListBandit *current, Info *info)
{
    ListBandit *new_maillon, *temp_maillon;

    new_maillon = bandit_new_maillon (new_bandit, NULL, info);
    temp_maillon = bandit_get_last_chain (current);
    temp_maillon->next = new_maillon;

    return current;
}

/* Add a new Bandit in the list inside info */
void bandit_add_to_list (Info *info, NextBandit info_neko)
{
    Bandit new_bandit;

    if (info->size_list < 0)
    {
        info->next_bandit.is_neko = FALSE;
        init_new_bandit (&info->mausu, info_neko);
        info->size_list++;
    }
    else if (info->size_list == 0)
    {
        info->next_bandit.is_neko = TRUE;
        init_new_bandit (& new_bandit, info_neko);
        info->nekos = bandit_new_maillon (new_bandit, NULL, info);
    }
    else
    {
        info->next_bandit.is_neko = TRUE;
        init_new_bandit (& new_bandit, info_neko);
        info->nekos = bandit_add (new_bandit, info->nekos, info);
    }
}
