/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "conf.h"


/* ===================== C O L O R S   U T I L S ==================== */

/* Set the given color channels as main color */
void set_color (GdkGC *gc, int r, int g, int b)
{
    GdkColor c;

    c.pixel = 0;
    c.red = r << 8;
    c.green = g << 8;
    c.blue = b << 8;
    gdk_gc_set_rgb_fg_color (gc, &c);
}

/* Put an hexa color into three color channels */
void change_color (GdkGC *gc, int hexcode)
{
    int color[3];

    color[0] = (hexcode & 0xFF0000) >> 16;
    color[1] = (hexcode & 0x00FF00) >> 8;
    color[2] = (hexcode & 0x0000FF);

    set_color(gc, color[0], color[1], color[2]);
}

/* GdkColor ---> RGB */
guint rgb_from_color (GdkColor *color)
{
    return (color->red   & 0xFF00) << 8
         | (color->green & 0xFF00)
         | (color->blue  & 0xFF00) >> 8;
}

/* RGB ---> GdkColor */
GdkColor *static_color_from_rgb (int hexcode)
{
    static GdkColor color;

    color.red   = (hexcode & 0xFF0000) >> 8;
    color.green = (hexcode & 0x00FF00);
    color.blue  = (hexcode & 0x0000FF) << 8;

    return & color;
}


/* ======================= T E X T   U T I L S ====================== */

/* Print ongoing stats for a given Bandit */

char *util_algo_name (int algo_num)
{
    char *algo_name = malloc (sizeof (char) * 10);
    switch (algo_num)
    {
        case UCB_1 : strcpy (algo_name, "UCB1"); break;
        case UCB_2 : strcpy (algo_name, "UCB2"); break;
        case E_GLO : strcpy (algo_name, "E-Glouton"); break;
        case E_GRE : strcpy (algo_name, "E-Greedy"); break;
        case EXP_3 : strcpy (algo_name, "EXP3"); break;
        default    : break;
    }

    return algo_name;
}

void
print_sub_result (Bandit *neko, GtkWidget *widget, GdkGC *gc,
                  PangoLayout *playout, int neko_num)
{
    char *algo_name;
    if (neko->winner)
    {
        font_draw_text (widget, gc, playout, FONT_TL, 10, 5,
                        "Neko number %d won!\n", neko_num);
    }

    if (neko->algo == UCB_2)
    {
        font_draw_text (widget, gc, playout, FONT_TL, 10,
                    30 + neko_num * 16,
                    "Neko %d: UCB2 (alpha : %.2f): time %d, (%d, %d)",
                        neko_num, neko->alpha, time_from_epoch (neko),
                        neko->pos.x, neko->pos.y);
    }
    else if (neko->algo < UCB_1 || neko->algo > 5)
    {
        font_draw_text (widget, gc, playout, FONT_TL, 10,
                        30 + neko_num * 16,
                        "Neko wrongly defined (bad algorithm number");
    } 
    else
    {
        algo_name = util_algo_name (neko->algo);
        font_draw_text (widget, gc, playout, FONT_TL, 10,
                        30 + neko_num * 16,
                        "Neko %d: %s (alpha : %.2f): time %.2d, (%d, %d)",
                        neko_num, algo_name, neko->alpha, neko->time,
                        neko->pos.x, neko->pos.y);
        free (algo_name);
    }
}

/* ======================= M A T H   U T I L S ====================== */

/* Rand function with float value */
float drand (void)
{
    return (float)rand () / RAND_MAX;
}

/* Take the minimum from 2 parameter */
int imin_abs (int a, int b)
{
    return (abs (a) < abs (b)) ? a : b;
}

/* Take the minimum from a table */
float minimum (float tab[], int n)
{
    int cptr;
    float min;
    min = tab[0];

    for(cptr = 0; cptr < n; cptr++)
    {
        if (tab[cptr] < min)
            min = tab[cptr];
    }

    return min;
}


/* =================== D I S T A N C E   U T I L S ================== */

/* Distance function between a (x,y) neko and a (x,y) mausu */
float dist (int x_neko, int y_neko, int x_mausu, int y_mausu)
{
    return sqrt(
                (x_neko - x_mausu) * (x_neko - x_mausu) +
                (y_neko - y_mausu) * (y_neko - y_mausu));
}

/* Check the lowest oriented distance from pos1 to pos2 on one
 * coordinate set (x or y) in a world with linked edge
 * */
int dist_warp (int pos1, int pos2, int world_length)
{
    int temp_val[3];

    temp_val[0] = pos2 - pos1;
    temp_val[1] = pos2 - pos1 - world_length;
    temp_val[2] = pos2 - pos1 + world_length;
    
    return imin_abs (temp_val[0], imin_abs (temp_val[1], temp_val[2]));
}


/* ====================== A N G L E   U T I L S ===================== */

float get_angle (Position orig, Position dest)
{
    float angle;
    Position current;
    
    current.x = dest.x - orig.x;
    current.y = dest.y - orig.y;
    
    angle = acos (current.x / sqrt (current.x * current.x +
                                    current.y * current.y));
                                                        
    return angle;
}

float get_best_angle (float angle1, float angle2)
{
    if (2 * PI - fabs (angle1 - angle2) <= fabs (angle1 - angle2))
    {
        return 2 * PI - fabs (angle1 - angle2);
    }
    else
    {
        return fabs (angle1 - angle2);
    }
}


/* ===================== O T H E R S   U T I L S ==================== */

/* Change the Position of the bandit's tail */
void update_tail (Bandit *b)
{
    int i;

    for (i = MAX_TAIL - 1; i > 0 ; i--)
    {
        b->tail[i].x = b->tail[i - 1].x;
        b->tail[i].y = b->tail[i - 1].y;
    }
    b->tail[0].x = b->pos.x;
    b->tail[0].y = b->pos.y;
}

/* Get the total time that the bandit running on UCB2 is launched out
 * of its personal epoch time
 * */
int time_from_epoch (Bandit *b)
{
    int total_time;
    int i;

    total_time = 0;

    if (b->algo != UCB_2)
    {
        return b->time;
    }

    for (i = 0; i < b->arm; i++)
    {
        total_time += b->param[i].epoch;
    }

    return total_time;
}
