/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BANDIT_H__
#define __BANDIT_H__

#include "conf.h"

/* Game mechanism */
void bandit_game (Info *info);
void bandit_choose_algorithm (Bandit *b, Info *info);
void bandit_reward_update (Bandit *neko, Info *info);
void bandit_reward (Bandit *b, int i);
void bandit_arm_update(Bandit *b, int i);
float bandit_reward_neko (Bandit *neko, Position after_move, Info *info);
float bandit_reward_mausu (Position pos_before, Position pos_after, Info *info);
void bandit_move (Bandit *neko, Info *info);

/* Initialisation */
Parameters init_bandit_parameters (void);
void init_new_bandit (Bandit *b, NextBandit info_neko);
void init_bandit_info_toolbar (Info *info);
void init_bandit_info (Info *info);

/* Free */
void bandit_info_free (Info *info);

/* Linked list */
ListBandit *bandit_get_last_chain (ListBandit *current);
Bandit *bandit_get_number (ListBandit *current, int number);
ListBandit *bandit_new_maillon (Bandit new_bandit, ListBandit *adress, Info *info);
ListBandit *bandit_add (Bandit new_bandit, ListBandit *current, Info *info);
void bandit_add_to_list (Info *info, NextBandit info_neko);
void bandit_list_free (ListBandit *adress, Info *info);

#endif
