/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <gtk/gtk.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define MAX_ARMS 18
#define NB_ALGO 5
#define TIMETOPLAY 1
#define SIZE 10
#define DEFAULT_SPEED 7
#define MAX_TAIL 10
#define MAX_BANDIT_NUM 500
#define BANDIT_MEMORY 250
#define PI 3.14159265359
#define TIMEOUT 30
#define HOMOGENEOUS 1
#define EXPAND 1
#define FILL 1
#define U __attribute__((unused))
#define ON_SIGNAL(w,n,c,d) \
    (g_signal_connect(G_OBJECT(w), (n), G_CALLBACK(c), (d) ))


typedef enum { false, true } bool;

typedef enum algo_ {
    MISSINGNO,
    UCB_1,                /* Upper Confidence Bound 1 */
    UCB_2,                /* Upper Confidence Bound 2 */
    E_GLO,                /* E-Glouton */
    E_GRE,                /* E-Greedy */
    EXP_3                 /* Exploration, Exploitation, Exponential */
} Algo;

typedef struct parameters_
{
    float win;            /* Win value */
    int choose;           /* How much the arm is choosen */
    float profit;         /* Total profit */
    int epoch;            /* Used by UCB2 */
    float weights;        /* Used by EXP3 */
} Parameters;

typedef struct position_
{
    int x;                /* X position */
    int y;                /* Y position */
} Position;

typedef struct bandit_
{
    Parameters param [MAX_ARMS];    /* Arms parameters */
    Position tail [MAX_TAIL];       /* Tail table size */
    Position pos;                   /* Bandit positions */
    Algo algo;                      /* Choosen algorithm */
    int arm;                        /* Number of arm */
    int color;                      /* Color used on the GUI to show this bandit */
    int time;                       /* Own bandit uptime */
    int last_arm_played;            /* Last used arm ID used for play functions */
    int speed;                      /* Speed of the bandit */
    int memory_index;               /* Last used memory index on the memory table */
    float memory[BANDIT_MEMORY][2]; /* Memory table that store old profit */
    float alpha;                    /* Alpha value used on algorithms */
    bool winner;                    /* bandit is the winner or not */ 
    bool is_neko;                   /* bandit is a neko or not */
} Bandit;

typedef struct listbandit_
{
    Bandit neko;                    /* Bandit of this node */
    struct listbandit_ *next;       /* Next node */
} ListBandit;

/* Temporarily store next neko's informations from the menu bar */
typedef struct nextneko_
{
    Algo algo;
    int speed;
    float alpha;
    int arm;
    int color;
    Position pos;
    bool is_neko;
} NextBandit;

typedef struct info_
{
    ListBandit *nekos;          /* List of all accessible Nekos(cats) */
    Bandit mausu;                /* The bandit to kill */
    int timeout;                /* Timeout until next game step is executed */
    int size_list;              /* Number of Nekos(cats) actually on the linked list */
    int area_width, area_height;/* Screen size, auto-updated */
    bool anim;                  /* Animation status for the start button */
    NextBandit next_bandit;     /* Next neko informations displayed on the top bar */
} Info;

/* General layout with a drawing area, a menu and the main box which
 * contain them all.
 */
typedef struct layout_
{
    GtkWidget *main_box;
    GtkWidget *area, *top_box;
    GtkWidget *start_button, *next_button, *reset_button;
    GtkWidget *stat_scroll, *stat_box, *stat_area;
    GtkWidget *next_bandit_color;
    GtkWidget *next_bandit_speed;
    GtkWidget *next_bandit_arm;
    GtkWidget *next_bandit_algo;
    GtkWidget *next_bandit_alpha;
} Layout;

/* GUI struct with bandit, layout informations and a window widget */
typedef struct gui_
{
    Info *info;
    Layout layout;
    GtkWidget *window, *stat_window;
} Gui;


#include "algo.h"
#include "bandit.h"
#include "event.h"
#include "font.h"
#include "gui.h"
#include "util.h"

#endif
