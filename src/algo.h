/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ALGO_H__
#define __ALGO_H__

#include "conf.h"

/* Algorithms */
void UCB1 (Bandit *b, float exploration, Info *info);
void UCB2 (Bandit *b, float alpha, Info *info);
void eglouton (Bandit *b, float epsilon, Info *info);
void egreedy (Bandit *b, float c, float d, Info *info);
void EXP3 (Bandit *b, float gamma, Info *info);

/* Algorithm events used on UCB1, E-Glouton and E-Greedy */
int algo_best_try (Bandit *b, float exploration);
void algo_play (Bandit *b, int arm, Info *info);

/* Special algorithm events for UCB2 */
int algo_UCB2_tau (int r, float alpha);
int algo_UCB2_best_try (Bandit *b, float alpha);
void algo_UCB2_play (Bandit *b, int arm, float alpha);

#endif
