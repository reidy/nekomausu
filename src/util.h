/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#include "conf.h"

/* Color */
void set_color (GdkGC *gc, int r, int g, int b);
void change_color (GdkGC *gc, int hexcode);
guint rgb_from_color (GdkColor *color);
GdkColor * static_color_from_rgb (int hexcode);

/* Text */
int get_algorithm_number (int n);
float get_alpha_number (void);
void print_sub_result (Bandit *neko, GtkWidget *widget, GdkGC *gc, PangoLayout *playout, int neko_num);

/* Math */
float drand (void);
int imin_abs (int a, int b);
float minimum (float tab[], int n);

/* Distance */
float dist (int x_neko, int y_neko, int x_mausu, int y_mausu);
int dist_warp (int pos1, int pos2, int world_length);

/* Angle */
float get_angle (Position orig, Position dest);
float get_best_angle (float angle1, float angle2);

/* Other */
void update_tail (Bandit *b);
int time_from_epoch (Bandit *b);

#endif
