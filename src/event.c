/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "event.h"


/* =================== C A L L B A C K   E V E N T ================== */

/* Redraw the stat window */
gboolean
on_stat_area_expose_event (GtkWidget U *widget, GdkEventExpose U *event,
                           gpointer data)
{
    GdkGC *gc;
    PangoLayout *playout;
    Info *info;
    Bandit *neko;
    int cptr;

    gc = gdk_gc_new (widget->window);
    playout = gtk_widget_create_pango_layout (widget, NULL);
    info = data;

    /* Print stat informations */
    font_set_name (playout, "Times, 12");
    for (cptr = 0; cptr < info->size_list; cptr++)
    {
        neko = bandit_get_number (info->nekos, cptr);
        print_sub_result (neko, widget, gc, playout, cptr);
    }

    g_object_unref (G_OBJECT (playout));
    g_object_unref (gc);

    return TRUE;
}

gboolean
on_area_expose_event (GtkWidget U * widget, GdkEventExpose U * event,
                      gpointer data)
{
    GdkGC *gc;
    guint w, h;
    int cptr;
    Gui *gui;
    Info *info;

    gc = gdk_gc_new (widget->window);
    gui = data;
    info = gui->info;
    w = widget->allocation.width;
    h = widget->allocation.height;

    /* Background rectangle */
    change_color (gc, 0x222222);
    gdk_draw_rectangle (widget->window, gc, FILL, 0, 0, w, h);

    /* Draw every neko from the linked list */
    for (cptr = 0; cptr < info->size_list; cptr++)
    {
        draw_neko (info, widget, cptr);
    }

    /* Mausu drawing */
    if (info->size_list >= 0)
    {
        draw_mausu (& info->mausu, widget);
    }

    g_object_unref (gc);

    return TRUE;
}

gboolean
on_area_configure (GtkWidget U * area, GdkEventConfigure U * event,
                   gpointer data)
{
    Gui *g;
    Info *info;
    g = data;
    info = g->info;

    info->area_height = event->height;
    info->area_width  = event->width;
    printf ("Screen size: %d*%d.\n", info->area_height,
                                     info->area_width);

    return TRUE;
}

gboolean
on_area_button_press (GtkWidget U * widget, GdkEventButton * event,
                      gpointer U data)   
{
    Gui *gui;
    Info *info;
    gui = data;
    info = gui->info;

    info->next_bandit.pos.x = event->x;
    info->next_bandit.pos.y = event->y;

    on_next_bandit_add (info);

    if (info->next_bandit.is_neko)
    {
        printf ("New Neko set at (%.0f, %.0f).\n", event->x, event->y);
    }
    else
    {
        printf ("Mausu set at (%.0f, %.0f).\n", event->x, event->y);
    }

    on_invalidate_areas (gui);

    return TRUE;
}


/* ============== D R A W   N E K O S   A N D   P I K A ============= */

/* Select a neko from the list on info and draw it */
void draw_neko (Info *info, GtkWidget *widget, int cptr)
{
    Bandit *neko;    
    GdkGC *gc;

    gc = gdk_gc_new (widget->window);
    neko = bandit_get_number (info->nekos, cptr);

    draw_tail (widget, neko, gc);

    /* Draw the Neko over the tail */
    change_color (gc, neko->color);
    gdk_draw_arc (widget->window, gc, FILL, neko->pos.x - SIZE / 2,
                                            neko->pos.y - SIZE / 2,
                                            SIZE, SIZE, 0, 64 * 360);

    g_object_unref (gc);
}

void draw_mausu (Bandit *mausu, GtkWidget *widget)
{
    GdkGC *gc;
    gc = gdk_gc_new (widget->window);

    draw_tail (widget, mausu, gc);

    /* Draw the Mausu over the tail */
    change_color (gc, mausu->color);
    gdk_draw_arc (widget->window, gc, FILL, mausu->pos.x - SIZE / 2,
                                            mausu->pos.y - SIZE / 2,
                                            SIZE, SIZE, 0, 64 * 360);

    g_object_unref (gc);
}

/* Update and draw tail positions */
void draw_tail (GtkWidget *widget, Bandit *b, GdkGC *gc)
{
    GdkColor *color;
    Position p;
    int i, reduce_value;

    color = static_color_from_rgb (b->color);
    reduce_value = 0;
    update_tail (b);

    for (i = 0; i < MAX_TAIL; i++)
    {
        color->red = 85 * color->red     / 100;
        color->green = 85 * color->green / 100;
        color->blue = 85 * color->blue     / 100;

        change_color (gc, rgb_from_color (color));

        p.x = b->tail[i].x;
        p.y = b->tail[i].y;

        gdk_draw_arc (widget->window, gc, FILL,
                      p.x - (SIZE - reduce_value) / 2,
                      p.y - (SIZE - reduce_value) / 2,
                      SIZE - reduce_value,
                      SIZE - reduce_value, 0, 64 * 360);

        if (reduce_value < MAX_TAIL)
        {
            reduce_value++;
        }
    }
}


/* ========================= A D D   N E K O  ======================= */

void on_next_bandit_add (Info *info)
{
    if (info->size_list >= MAX_BANDIT_NUM)
    {
        perror ("Max neko number limit reached on on_button_add_neko");
        return;
    }

    bandit_add_to_list (info, info->next_bandit);
}

void on_next_bandit_color (GtkColorButton U * button, gpointer data)
{
    GdkColor c;
    Gui *gui;
    Info *info;

    gui = data;
    info = gui->info;

    gtk_color_button_get_color (button, & c);
    info->next_bandit.color = rgb_from_color (& c);
}

void on_next_bandit_algo (GtkWidget *widget, gpointer data)
{
    Info *info;
    info = data;

    info->next_bandit.algo = gtk_combo_box_get_active (GTK_COMBO_BOX
                                                              (widget));
    info->next_bandit.algo += 1;
}

void on_next_bandit_alpha (GtkSpinButton U * spin, gpointer data)
{
    Gui *gui;
    Layout *l;
    NextBandit * next_bandit;

    gui = data;
    l = & gui->layout;
    next_bandit = & gui->info->next_bandit;

    next_bandit->alpha = gtk_spin_button_get_value (GTK_SPIN_BUTTON
                                                (l->next_bandit_alpha));
}

void on_next_bandit_arm (GtkSpinButton U * spin, gpointer data)
{
    Gui *gui;
    Layout *l;
    NextBandit * next_bandit;

    gui = data;
    l = & gui->layout;
    next_bandit = & gui->info->next_bandit;

    next_bandit->arm = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON
                                                  (l->next_bandit_arm));
}

void on_next_bandit_speed (GtkSpinButton U * spin, gpointer data)
{
    Gui *gui;
    Layout *l;
    NextBandit * next_bandit;

    gui = data;
    l = & gui->layout;
    next_bandit = & gui->info->next_bandit;

    next_bandit->speed = gtk_spin_button_get_value_as_int (
                                GTK_SPIN_BUTTON (l->next_bandit_speed));
}


/* ===================== B U T T O N   E V E N T ==================== */

void on_button_reset (GtkButton U * button, gpointer data)
{
    Gui *g = data;
    Info *info = g->info;

    if (info->size_list > 0)
    {
        if (info->anim)
        {
            on_button_start (NULL, g);
        }
        bandit_info_free (info);
        info->size_list = -1;
    }
    on_invalidate_areas (g);
}

void on_button_next (GtkButton U * button, gpointer data)
{
    Gui *g;
    Info *info;

    g = data;
    info = g->info;

    if (!info->anim)
    {
        if (info->size_list <= 0)
        {
            perror ("No bandit initialized. on_button_next");
            return;
        }
        bandit_game (info);
    }
    on_invalidate_areas (g);
}

void on_button_start (GtkButton U * button, gpointer data)
{
    Gui *g;
    Layout *l;
    Info *info;

    g = data;
    l = & g->layout;
    info = g->info;

    if (info->size_list <= 0)
    {
        perror ("No bandit initialized. on_button_start");
        return;
    }

    info->anim = !info->anim;
    (!info->anim)
        ? gtk_button_set_label (GTK_BUTTON (l->start_button), "Start")
        : gtk_button_set_label (GTK_BUTTON (l->start_button), "Stop");

    (info->anim) ? info_anim_start (info, data) : info_anim_stop (info);
}


/* ==================== T I M E O U T   E V E N T =================== */

gboolean on_timeout (gpointer data)
{
    Gui *g;
    Info *info;

    g = data;
    info = g->info;

    bandit_game (info);
    on_invalidate_areas (g);

    return TRUE;
}

void info_anim_start (Info *info, gpointer data)
{
    if (info->timeout == 0)
        info->timeout = g_timeout_add (TIMEOUT, on_timeout, data);
}

void info_anim_stop (Info *info)
{
    if (info->timeout != 0)
    {
        g_source_remove (info->timeout);
        info->timeout = 0;
    }
}


/* ====================== O T H E R   E V E N T ===================== */

/* Updated the queue and reload both screen */
void on_invalidate_areas (Gui *gui)
{
    gdk_window_invalidate_rect (gui->layout.area->window, NULL, FALSE);
    gdk_window_invalidate_rect (gui->layout.stat_area->window,
                                                          NULL, FALSE);
}
