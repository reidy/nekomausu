/*
 *  Copyright (C) 2015  Reid & Sykefu
 *
 *  This file is part of NekoMansu.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EVENT_H__
#define __EVENT_H__

#include "conf.h"

/* Callback event */
gboolean on_stat_area_expose_event (GtkWidget U * widget, GdkEventExpose U * event, gpointer data);
gboolean on_area_expose_event (GtkWidget U * widget, GdkEventExpose U * event, gpointer data);
gboolean on_area_configure (GtkWidget U * area, GdkEventConfigure U * event, gpointer data);
gboolean on_area_button_press (GtkWidget U * widget, GdkEventButton * event, gpointer U data);

/* Draw bandits */
void draw_neko (Info *info, GtkWidget *widget, int cptr);
void draw_mausu (Bandit *mausu, GtkWidget *widget);
void draw_tail (GtkWidget *widget, Bandit *b, GdkGC *gc);

/* Add bandit */
void on_next_bandit_add (Info *info);
void on_next_bandit_color (GtkColorButton U * button, gpointer data);
void on_next_bandit_algo (GtkWidget *widget, gpointer data);
void on_next_bandit_alpha (GtkSpinButton U * spin, gpointer data);
void on_next_bandit_arm (GtkSpinButton U * spin, gpointer data);
void on_next_bandit_speed (GtkSpinButton U * spin, gpointer data);

/* Button event */
void on_button_reset (GtkButton U * button, gpointer data);
void on_button_next (GtkButton U * button, gpointer data);
void on_button_start (GtkButton U * button, gpointer data);

/* Timeout event */
gboolean on_timeout (gpointer data);
void info_anim_start (Info *info, gpointer data);
void info_anim_stop (Info *info);

/* Other event */
void on_invalidate_areas (Gui *gui);

#endif
