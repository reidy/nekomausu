CC       = gcc
CFLAGS   = $$(pkg-config gtk+-2.0 --cflags) -Wall -Wextra -ansi -pedantic
LIBS     = $$(pkg-config gtk+-2.0 --libs)  -Wall -Wextra -ansi -pedantic -lm
RM       = rm -f

SRCDIR   = src
OBJDIR   = obj
BINDIR   = ./
OUTDIR   = outputs

SOURCES  = $(wildcard $(SRCDIR)/*.c)
INCLUDES = $(wildcard $(SRCDIR)/*.h)
OBJECTS  = $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
BINARY   = $(BINDIR)/NekoMausu

all: $(BINARY)

$(BINARY): $(OBJECTS)
	@$(CC) $(LIBS) $^ -o $@
	@echo "Compiled" $@ "successfully."

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "(CC) "$<""

.PHONY: clean
clean:
	@$(RM) $(OBJECTS)
	@echo "Objects removed."

.PHONY: remove
remove: clean
	@$(RM) $(BINDIR)/$(BINARY)
	@echo "Executable removed."
