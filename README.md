# Neko & Mausu

UCB Algorithms and Adversarial Bandit problem illustrated by a hunt
game with a mouse called Mausu and an army of clone cat called Neko.


## Libraries
### Library used
 - [Font from Ez-Draw (by Edouard Thiel)]

### Libraries needed
 - Gtk+ 2
 - Pango


## Introduction
This game introduces an army of cats chasing a mouse, all of them are
using different Adversarial Bandit algorithms (UCB1, UCB2, E-Glouton,
E-Greedy and EXP3).

You will be able to add new bandits anytime, but, only the first one
will be a Mausu, all other will then be Neko, choose well your stats!

The Mausu, is meant to be set with an average setting to let its
opponent a chance to win. Its stats should usually be:  
Speed:         7  
Alpha:         0.01  
Arm/direction: 10

As for Nekos' settings, these parameters are up to you. You can choose
a color, an algorithm
and its alpha value, the speed and the number of direction (arms) that
this hunter can use.

These Adversarial Bandit use a regret notion based on the hundred
previous moves. Older moves has less influance that newer moves.

## Statistic
Displayed stats are average results of ~10 tries, a game's tic is equal
to 30ms, values expressed below are game's tic.

| Alpha | 0.01 | 0.03 | 0.06 | 0.1 | 0.2 | 0.3 | 0.5 | 0.7 | 0.9 |
|:------|:-----|:-----|:-----|:----|:----|:----|:----|:----|:----|
| UCB1  | 1269 |  322 |  385 | 173 | 164 | 223 | 258 | 214 | 168 |
| UCB2  |  942 |  294 |  286 | 166 | 187 | 285 | 345 | 334 | 287 |

| Alpha     | 0.05 | 0.1 | 0.2 | 0.3 | 0.4 | 0.5 | 0.6 | 0.7  | 0.9  |
|:----------|:-----|:----|:----|:----|:----|:----|:----|:-----|:-----|
| E-Glouton |  156 | 134 | 207 | 243 | 182 | 243 | 658 | 1337 | 1828 |

| Alpha    | 0.5 | 0.8 |  1  |  2  |  3  |  4  |  5  |  7  |  9  |
|:---------|:----|:----|:----|:----|:----|:----|:----|:----|:----|
| E-Greedy | 810 | 327 | 232 | 298 | 328 | 691 | 393 | 244 | 170 |

| Alpha | 0.05 | 0.08 | 0.1 | 0.2 | 0.3 | 0.4 | 0.5 | 0.7 | 0.9  |
|:------|:-----|:-----|:----|:----|:----|:----|:----|:----|:-----|
| EXP3  |  678 |  703 | 606 | 272 | 537 | 674 | 873 | 868 | 1209 |

## Code style
C ANSI.  
Code is intended with 4 spaces, no tabs.  
A line should not exceed 72 characters.  
It follows Allman indent style.


## Documentation
[UCB Algorithm and Adversarial Bandit Problem lecture from the University of Michigan]

## Screenshots
![Screen1](data/screen1.png)
![Screen2](data/screen2.png)

## Graphic stats
![stat](data/stat.png)

[UCB Algorithm and Adversarial Bandit Problem lecture from the University of Michigan]:http://web.eecs.umich.edu/~jabernet/eecs598course/web/notes/lec19_111113.pdf
[Font from Ez-Draw (by Edouard Thiel)]:http://pageperso.lif.univ-mrs.fr/~edouard.thiel/ez-draw/index-en.html
